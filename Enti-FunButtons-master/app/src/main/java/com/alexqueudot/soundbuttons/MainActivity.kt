package com.alexqueudot.soundbuttons

import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Button
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnItemClickListener<SoundModel> {

    var mediaPlayer: MediaPlayer? = null;
    var firebaseAnalytics: FirebaseAnalytics? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(this, "ca-app-pub-9127568573814410~1157045175")
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Get JSON String
        var jsonString = resources.openRawResource(R.raw.sounds).bufferedReader().use { it.readText() }


        //Create List
        var gson = Gson()
        val soundList = gson.fromJson(jsonString, SoundList::class.java)


        soundList.sounds?.let {
            Log.i("MainACtivity", "Sounds length: "+it.count())
            // Set Layout Manager
            soundsRecyclerview.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            // Create Adapter
            var adapter = SoundsAdapter(it)
            adapter.onSoundClickListener = this
            // Assign Adapter
            soundsRecyclerview.adapter = adapter

        }


//        button.setOnClickListener {
//            // Play first song
//            mediaPlayer?.release()
//
//            mediaPlayer = MediaPlayer.create(this, R.raw.rick)
//            mediaPlayer?.start() // no need to call prepare(); create() does that for you
//        }
//
//        button2.setOnClickListener {
//            // Play second song
//            mediaPlayer?.release()
//            mediaPlayer = MediaPlayer.create(this, R.raw.hog_rider)
//            mediaPlayer?.start() // no need to call prepare(); create() does that for you
//        }
    }

    override fun onItemClick(item: SoundModel, position: Int) {
        mediaPlayer?.release()
        mediaPlayer = MediaPlayer.create(this, resources.getIdentifier(item.file,"raw", packageName))
        mediaPlayer?.start()

        val bundle = Bundle();
        bundle.putString("sound_title", item.title);


       firebaseAnalytics?.logEvent("sound_play", bundle);
    }

    override fun onStop() {
        super.onStop()
        mediaPlayer?.release()
        mediaPlayer = null
    }
}
