package com.alexqueudot.soundbuttons

/**
 * Created by alex on 09/11/2018.
 */

data class SoundModel(
    var id: Int?,
    var title: String?,
    var file: String?
)

data class SoundList(
    var sounds: ArrayList<SoundModel>?
)